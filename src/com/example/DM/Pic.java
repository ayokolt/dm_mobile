package com.example.DM;

import java.util.ArrayList;

/**
 * Created by amaizo on 01/06/16.
 */
public class Pic {
    private String id;
    private String name;
    private String date;
    private ArrayList<Tag> tags;

    Pic(String id, String date){
        this.id = id;
        this.name = id;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate(){
        return this.date;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void addTag(Tag t){
        this.tags.add(t);
    }

    public void delTag(Tag t){
        this.tags.remove(t);
    }
}
