package com.example.DM;

/**
 * Created by amaizo on 01/06/16.
 */
public class Tag {

    private int id;
    private String name;

    Tag(int id, String name){
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
}
